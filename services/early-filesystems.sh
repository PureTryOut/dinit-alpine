#!/bin/sh

if [ "$1" = start ]; then
	PATH=/usr/bin:/usr/sbin:/bin:/sbin

	mount -n -t tmpfs -o mode=775 tmpfs /run

	# Nothing else is needed, initramfs has set up other early filesystems already
fi
