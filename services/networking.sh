#!/bin/sh

: ${cfgfile:="/etc/network/interfaces"}
: ${ifquery:="ifquery"}

find_ifaces() {
	if command -v "$ifquery" > /dev/null; then
		$ifquery -i "$cfgfile" --list -a
		return
	fi

	# fallback in case ifquery does not exist
	awk '$1 == "auto" {for (i = 2; i <= NF; i = i + 1) printf("%s ", $i)}' "$cfgfile"
}

for iface in $(find_ifaces); do
	r=0
	echo "$iface"
	if ! ifup -i "$cfgfile" $iface; then
		ifdown -i "$cfgfile" $iface
		r=1
	fi

	# We need at least one valid interface
	if [ $r != 0]; then
		exit $r
	fi
done
